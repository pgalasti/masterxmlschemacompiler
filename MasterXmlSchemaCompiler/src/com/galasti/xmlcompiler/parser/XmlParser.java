package com.galasti.xmlcompiler.parser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.galasti.xmlcompiler.structure.Attribute;
import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;
import com.galasti.xmlcompiler.structure.document.XmlDocument;
import com.galasti.xmlcompiler.structure.document.XmlDocumentReader;

/**
 * Parses an XML structured file. Will return a reader object so elements can
 * be grabbed from an object representing the XML structured file.
 * @author pgalasti
 *
 */
public class XmlParser {

	private File file;

	/**
	 * Constructs an XmlParser object with a given file. File type must be XSD, XML, or WSDL.
	 * @param file An XML file.
	 */
	public XmlParser(File file) {
		this.file = file;

		final String fileExtension = this.getFileExtension();

		if(!fileExtension.equalsIgnoreCase("XSD") &&
				!fileExtension.equalsIgnoreCase("XML") &&
				!fileExtension.equalsIgnoreCase("WSDL")) {
			throw new RuntimeException("XmlParser requires a file type of xml, xsd, wlsd");
		}
	}

	/**
	 * Clean and parses an XML file into a {@link XmlDocument} type. 
	 * @return An XmlDocument object with cleaned text.
	 */
	public XmlDocumentReader parseFile() {

		// Clean up the current text of the XML document.
		Document cleanedDocument = this.cleanupXmlDocument();

		// Create the root element.
		XmlRootElement rootElement = new XmlRootElement(cleanedDocument.getXmlVersion(), cleanedDocument.getInputEncoding());

		// Parse out the elements.
		org.w3c.dom.Element rootNode = cleanedDocument.getDocumentElement();
		XmlBodyElement rootBodyElement = extractElements(rootNode);

		// Put into a document.
		XmlDocumentReader document = new XmlDocumentReader(rootElement, rootBodyElement);
		return document;
	}

	private Document cleanupXmlDocument() {

		DocumentBuilder builder = null;
		StringWriter stringWriter = null;
		Document cleanedDocument = null;
		
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			
			Document doc = builder.parse(this.file);
			
			// Get the encoding
			final String encodingType = doc.getInputEncoding();
			
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			// Setup the transformer properties
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, encodingType);
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			// Transform into string writer.
			stringWriter = new StringWriter();
			transformer.transform(new DOMSource(doc), 
					new StreamResult(stringWriter));

			// Parse the cleaned string and get the cleaned Document
			cleanedDocument = builder.parse(new ByteArrayInputStream(stringWriter.toString().getBytes()));
			
		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
			throw new RuntimeException("Failure to clean up XML Document.", e);
		}
		
		return cleanedDocument;
	}
	
	private XmlBodyElement extractElements(Node node) {

		// Grab the attributes for the element.
		XmlBodyElement element = new XmlBodyElement(node.getNodeName());
		element = this.extractAttributes(node, element);

		// Iterate through node child list.
		final NodeList childList = node.getChildNodes();
		for(int i = 0; i < childList.getLength(); i++) {

			final Node childNode = childList.item(i);

			// Check if the element has text content
			if(childNode.getNodeType() == Node.TEXT_NODE) {

				// Check if there's actual text as content, put into element content
				final String content = childNode.getTextContent().trim();
				if(!content.isEmpty()) {
					element.setContent(content);
				}
			}

			// Check if the node is another actual element
			if(childNode.getNodeType() == Node.ELEMENT_NODE) {
				
				// Recursively parse child nodes and add to this element.
				XmlBodyElement childElement = this.extractElements(childNode);
				element.addElement(childElement);
			}
		}

		return element;
	}

	private XmlBodyElement extractAttributes(Node node, XmlBodyElement element) {

		// Add the attributes to the element and return it
		List<Node> attributeList = this.getAttributes(node);
		for(Node attribute : attributeList) {
			element.addAttribute(new Attribute(attribute.getNodeName(), attribute.getNodeValue()));
		}

		return element;
	}

	private List<Node> getAttributes(Node node) {

		ArrayList<Node> list = new ArrayList<>();

		// Iterate through the node map and add to the list
		NamedNodeMap map = node.getAttributes();
		for(int i = 0; i < map.getLength(); i++) { // Elements are stacked decending from original order
			list.add(map.item(i));
		}

		return list;
	}

	private String getFileExtension() {

		// Find the last instance of '.' and return the extension
		final String fileName = this.file.getName();
		final int extensionStart = fileName.lastIndexOf('.')+1;
		final String extension = fileName.substring(extensionStart);

		return extension;
	}
}
