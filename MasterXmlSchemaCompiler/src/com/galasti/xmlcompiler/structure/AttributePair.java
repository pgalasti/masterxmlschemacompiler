package com.galasti.xmlcompiler.structure;

/**
 * Represents an XML attribute pair
 * @author pgalasti
 *
 */
public class AttributePair {

	private String name;
	private String value;
	
	/**
	 * Gets the name of the attribute pair.
	 * @return Name of the attribute pair
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Sets the name of the attribute. Cannot be blank or with any whitespaces
	 * @param name Name of the attribute pair
	 */
	public void setName(String name) {
		if(name.contains(" ") || name.isEmpty()) {
			throw new RuntimeException(
					"An attribute name cannot have a whitespace character or be empty!");
		}
		
		this.name = name;
	}
	
	/**
	 * Returns the value of the attribute pair
	 * @return The attribute value
	 */
	public String getValue() {
		return this.value;
	}
	
	/**
	 * Sets the value of the attribute
	 * @param value The value of the attribute
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the quoted value <"value">
	 * @return The value in string representation in quotes
	 */
	public String getQuotedValue() {
		return new StringBuffer("\"")
		.append(value)
		.append("\"").toString();
	}

	/**
	 * Returns a string representation of an attribute pair <name="value">
	 * @return The string representation of an attribute pair
	 */
	public String getPair() {
		return new StringBuffer(this.name)
		.append("=")
		.append(getQuotedValue()).toString();
	}
}
