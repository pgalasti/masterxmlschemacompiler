package com.galasti.xmlcompiler.structure;

/**
 * Represents a tag of an XML element.
 * @author pgalasti
 *
 */
public class Tag {

	protected static char NAMESPACE_SEPARATOR = ':';
	
	protected String text;
	protected String namespace;
	
	public Tag(String namespace, String text) {
		this.namespace = namespace;
		this.text = text;
	}
	
	public Tag(String text) {
		this.namespace = "";
		this.text = text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	public String getNamespace() {
		return this.namespace;
	}
	
	public boolean compare(Tag otherTag) {
		
		if(this.getNamespace().compareToIgnoreCase(otherTag.getNamespace()) == 0 &&
				this.getText().compareToIgnoreCase(otherTag.getText()) == 0)
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		if(namespace.isEmpty()) {
			return this.text;
		}
		
		return new StringBuilder(this.namespace).append(NAMESPACE_SEPARATOR).append(this.text).toString();
	}
}
