package com.galasti.xmlcompiler.structure;

/**
 * Represents an XML element attribute
 * @author pgalasti
 *
 */
public class Attribute {

	private AttributePair attributePair;
	
	/**
	 * Constructs an attribute with a name and blank value.
	 * @param name Name of the attribute
	 */
	public Attribute(String name) {
		this.attributePair = new AttributePair();
		this.attributePair.setName(name);
	}
	
	/**
	 * Constructs an attribute with name and value pair.
	 * @param name Name of the attribute
	 * @param value Value of the attribute
	 */
	public Attribute(String name, String value) {
		this.attributePair = new AttributePair();
		this.attributePair.setName(name);
		this.attributePair.setValue(value);
	}
	
	/**
	 * Creates an attribute from another attribute
	 * @param attribute The other attribute values to construct this attribute
	 */
	public Attribute(Attribute attribute) {
		this.attributePair = new AttributePair();
		this.attributePair.setName(attribute.getName());
		this.attributePair.setValue(attribute.getValue());
	}
	
	/**
	 * Sets the value of an attribute
	 * @param value The new value of the attribute
	 */
	public void setValue(String value) {
		this.attributePair.setValue(value);
	}
	
	/**
	 * Gets the value of the attribute.
	 * @return The value of the attribute.
	 */
	public String getValue() {
		return this.attributePair.getValue();
	}
	
	/**
	 * Get the name of the attribute
	 * @return The name of the attribute
	 */
	public String getName() {
		return this.attributePair.getName();
	}
	
	/**
	 * Gets a string representation of an attribute <name="value">
	 * @return String representation of an attribute
	 */
	public String getAttribute() {
		return this.attributePair.getPair();
	}
}
