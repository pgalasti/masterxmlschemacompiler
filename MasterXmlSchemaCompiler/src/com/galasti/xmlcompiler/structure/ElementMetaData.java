package com.galasti.xmlcompiler.structure;

import com.galasti.xmlcompiler.settings.Settings;

/**
 * Represents how an element should be formatted as output
 * @author pgalasti
 *
 */
public class ElementMetaData {

	private int tabDepth;
	public int line;
	private boolean doTabDepth;
	
	public ElementMetaData() {
		this.doTabDepth = false;
		
		if(Settings.getTabDepthEnable() == 'Y' || Settings.getTabDepthEnable() == 'y') {
			this.doTabDepth = true;
		}
	}
	
	
	/**
	 * Increments the tab depth of the meta data structure
	 */
	public void incrementTabDepth() {
		if(this.doTabDepth) {
			this.tabDepth++;
		}
	}
	
	/**
	 * Decrements the tab depth of the meta data structure
	 */
	public void decrementTabDepth() {
		if(this.doTabDepth) {
			this.tabDepth--;
		}
	}
	
	/**
	 * Returns the tab depth of the meta data structure
	 * @return Tab depth of the meta data structure
	 */
	public int getTabDepth() {
		return this.tabDepth;
	}
}
