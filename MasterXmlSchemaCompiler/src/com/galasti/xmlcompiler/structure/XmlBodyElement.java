package com.galasti.xmlcompiler.structure;

/**
 * Represents any element in an XML formatted structure that is not the top 
 * most xml element.
 * @author pgalasti
 *
 */
public class XmlBodyElement extends Element {

	protected Tag tag;
	
	/**
	 * Constructs a new XmlBodyElement with a tag object.
	 * @param tag
	 */
	public XmlBodyElement(Tag tag) {
		this.tag = new Tag(tag.getNamespace(), tag.getText());
	}
	
	/**
	 * Constructs a new XmlBodyElement with a tag text and namespace.
	 * @param Namespace namespace of element
	 * @param tag Tag of the element
	 */
	public XmlBodyElement(String namespace, String tag) {
		this.tag = new Tag(namespace, tag);
	}
	
	/**
	 * Constructs a new XmlBodyElement with a tag text and blank namespace.
	 * @param tag Tag of the element
	 */
	public XmlBodyElement(String tag) {
		
		this.tag = new Tag("", "");
		
		if(tag.contains(":")) {
			final int dividerPosition = tag.lastIndexOf(':');
			
			this.tag.setNamespace(tag.substring(0, dividerPosition));
			this.tag.setText(tag.substring(dividerPosition+1, tag.length()));
		} else {
			this.tag.setText(tag);
		}
	}
	
	@Override
	public String toString(ElementMetaData elementMetaData) {
		elementMetaData.incrementTabDepth();
		
		// Start our tag
		StringBuilder builder = new StringBuilder("<").append(this.tag);
		
		// Add attributes if they exist
		for(Attribute attribute : this.attributes.values()) {
			builder.append(" ")
			.append(attribute.getAttribute());
		}
		builder.append(">");

		// If there are no child elements, just write out the content.
		// Otherwise, write out the elements.
		if(this.childElements.isEmpty()) {
			builder.append(this.getContent());
		} else {
			
			// Sanity check
			if(!this.getContent().isEmpty()) {
				throw new RuntimeException(
						"An XML body element cannot have content and other elements!");
			}
			
			for(Element element : this.childElements) {
				
				builder.append("\n").append(Element.getTabs(elementMetaData));
				builder.append(element.toString(elementMetaData));
				
			}
		}
		
		elementMetaData.decrementTabDepth();
		
		// Make formatting pretty; maybe add as a .property option
		if(!this.childElements.isEmpty()) {
			builder.append("\n").append(Element.getTabs(elementMetaData));
		}
		
		// Close our element tag
		builder.append("</").append(this.tag).append(">");
		return builder.toString();
	}
	
	/**
	 * Sets the tag of the element
	 * @param tag Tag of the element
	 */
	public void setTagText(String tag) {
		this.tag.setText(tag);
	}
	
	public void setTagNamespace(String namespace) {
		this.tag.setNamespace(namespace);
	}
	
	/**
	 * Returns the tag of the element.
	 * @return Tag of the element
	 */
	public String getTagText() {
		return this.tag.getText();
	}

	/**
	 * Returns the element tag object.
	 * @return The element tag object
	 */
	public Tag getTag() {
		return this.tag;
	}
	
	/**
	 * Recursively searches this element and children elements for the 
	 * specified XmlBodyElement. If the element is not a part of the element
	 * chain, it will return null. 
	 * 
	 * @param otherElement The element to be searched for int he element chain
	 * @return The found XmlBodyElement if found, otherwise null.
	 */
	public XmlBodyElement find(XmlBodyElement otherElement) {
		
		if(this.equals(otherElement)) {
			return this;
		}
		
		for(Element element : this.childElements) {
			XmlBodyElement childElement = ((XmlBodyElement)element);
			
			if(childElement.equals(otherElement)) {
				return childElement;
			}
			
			childElement.find(otherElement);
		}
		
		return null;
	}
	
	/**
	 * Recursively searches this element and children elements for the 
	 * specified tag text. If the element is not a part of the element
	 * chain, it will return null. 
	 * @param tag Tag to search for
	 * @return The found XmlBodyElement if found, otherwise null.
	 */
	public XmlBodyElement find(String tagText) {
		
		if(this.tag.getText().compareToIgnoreCase(tag.getText()) == 0) {
			return this;
		}
		
		for(Element element : this.childElements) {
			XmlBodyElement childElement = ((XmlBodyElement)element);
			
			if(childElement.getTagText().equalsIgnoreCase(tag.getText())) {
				return childElement;
			}
			
			childElement.find(tag);
		}
		
		return null;
	}
	
	/**
	 * Recursively searches this element and children elements for the
	 * specified tag. If the element is not apart of the element chain
	 * it will return null.
	 * @param tag The tage object to search.
	 * @return The found XmlBodyElement if found, otherwise null.
	 */
	public XmlBodyElement find(Tag tag) {
		
		if(this.getTagText().compareToIgnoreCase(tag.getText()) == 0 &&
				this.tag.getNamespace().compareToIgnoreCase(tag.getNamespace()) == 0) {
			return this;
		}
		
		for(Element element : this.childElements) {
			XmlBodyElement childElement = ((XmlBodyElement)element);
			
			if(childElement.getTagText().equalsIgnoreCase(tag.getText()) &&
					childElement.tag.getNamespace().compareToIgnoreCase(tag.getNamespace()) == 0) {
				return childElement;
			}
			
			childElement.find(tag);
		}
		
		return null;
		
	}
	
	/**
	 * Removes the specified XmlBodyElement from the children if it exists.
	 * @param childToRemove Child element to be removed.
	 */
	public void remove(XmlBodyElement childToRemove) {
		this.childElements.remove(childToRemove);
	}
	
	@Override
	public XmlBodyElement clone() {
		
		XmlBodyElement rootElement = new XmlBodyElement(this.tag);
		
		// Get attributes
		rootElement.attributes.putAll(this.attributes);
		
		// Get content
		rootElement.setContent(this.content);
		
		// Recursively get body elements
		for(Element element : this.childElements) {
			rootElement.addElement(((XmlBodyElement)element).clone());
		}
		
		return rootElement;
	}

}
