package com.galasti.xmlcompiler.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An abstract class representing an XML structured element.
 * @author pgalasti
 *
 */
public abstract class Element {

	protected Map<String, Attribute> attributes;
	protected List<Element> childElements;
	
	protected String content;
	
	public Element() {
		this.attributes = new HashMap<>();
		this.childElements = new ArrayList<>();
		content = "";
	}
	
	/**
	 * Returns the content of an XML element.
	 * @return Content of an XML element
	 */
	public String getContent() {
		return this.content;
	}
	
	/**
	 * Sets the content of an XML element.
	 * @param content Content of an XML element
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Adds a child element to this element. 
	 * @param element Element to be added as a child
	 */
	public void addElement(Element element) {
		this.childElements.add(element);
	}
	
	/**
	 * Adds an attribute name/value pair to an XML element
	 * @param attribute An attribute name/value
	 */
	public void addAttribute(Attribute attribute) {
		Attribute newAttribute = new Attribute(attribute);
		this.attributes.put(attribute.getName(), newAttribute);
	}
	
	/**
	 * Finds an attribute with the specified name in the element. If the name 
	 * does not exist, will return null.
	 * @param name The name of an attribute to be found
	 * @return The attribute with the name, else null.
	 */
	public String findAttribute(String name) {
		final Attribute attribute = this.attributes.get(name);
		return attribute == null ? "" : attribute.getValue();
	}
	
	/**
	 * A list of child elements of this element.
	 * @return Child elements
	 */
	public List<Element> getChildElements() {
		return this.childElements;
	}
	
	/**
	 * A Set of attributes belonging to this element.
	 * @return Attributes of this element
	 */
	public Set<Attribute> getAttributes() {
		
		Set<Attribute> set = new HashSet<>();
		for(Attribute attribute : this.attributes.values()) {
			set.add(attribute);
		}
		
		return set;
	}
	
	/**
	 * Returns string format of an XML element output based on meta data
	 * @param elementMetaData Meta data to format an element output
	 * @return A formatted XML element based on element meta data.
	 */
	public abstract String toString(ElementMetaData elementMetaData);
	
	/**
	 * Returns the amount of tabs based on element meta data.
	 * @param elementMetaData Element meta data.
	 * @return Tabs based on element meta data.
	 */
	public static String getTabs(ElementMetaData elementMetaData) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < elementMetaData.getTabDepth(); i++) {
			builder.append("\t");
		}
		return builder.toString();
	}
}
