package com.galasti.xmlcompiler.structure.document;

import java.io.PrintWriter;

import com.galasti.xmlcompiler.structure.ElementMetaData;
import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;

/**
 * Extends off {@link XmlDocument} and implements set methods from 
 * {@link IXmlWriter} to write elements to the XmlDocument.
 * <br/>
 * @author pgalasti
 */
public class XmlDocumentWriter extends XmlDocument implements IXmlWriter {

	private String filePath;
	
	public XmlDocumentWriter(String filePath) {
		super(new XmlRootElement("", ""), new XmlBodyElement(""));
		this.setFilePath(filePath);
	}
	
	/**
	 * Constructs an XML document to be written to with elements.
	 * @param filePath The file path of the file once written.
	 * @param xmlRootElement The root element.
	 * @param xmlTopElement The top body element.
	 */
	public XmlDocumentWriter(String filePath, XmlRootElement xmlRootElement, XmlBodyElement xmlTopElement) {
		super(xmlRootElement, xmlTopElement);
		this.setFilePath(filePath);
	}
	
	/**
	 * Writes the elements to file.
	 */
	public void writeToFile() {

		PrintWriter writer = null;

		try {
			// Get file encoding from XML definion
			final String encoding = xmlRootElement.findAttribute(XmlRootElement.ENCODING_TAG);
			writer = new PrintWriter(this.filePath, encoding);

			ElementMetaData elementMetaData = new ElementMetaData();

			// Write out Root Element
			writer.write(this.xmlRootElement.toString(elementMetaData));
			writer.println();

			// Write out Body Element
			elementMetaData.line = 1;
			if(this.xmlTopElement != null) {
				writer.write(this.xmlTopElement.toString(elementMetaData));
			}

		} catch(Exception e) {
			throw new RuntimeException(e);
		} finally {
			writer.close();
		}
	}
	
	/**
	 * Sets the file path of the XML document to be written.
	 * @param filePath The file path of the XML document.
	 */
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	/**
	 * Sets the root element to the writer.
	 */
	@Override
	public void setRootElement(final XmlRootElement xmlRootElement) {
		this.xmlRootElement = xmlRootElement;
	}
	
	/**
	 * Sets the top body element to the writer.
	 */
	@Override
	public void setTopElement(final XmlBodyElement xmlBodyElement) {
		this.xmlTopElement = xmlBodyElement;
	}
}
