package com.galasti.xmlcompiler.structure.document;

import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;

/**
 * Interface to write elements to a document.
 * @author pgalasti
 *
 */
public interface IXmlWriter {

	/**
	 * Sets the root element to a document.
	 * @param xmlRootElement The root element.
	 */
	public void setRootElement(final XmlRootElement xmlRootElement);
	
	/**
	 * Sets the top body element to a document.
	 * @param xmlBodyElement The top body element.
	 */
	public void setTopElement(final XmlBodyElement xmlBodyElement);
	
}
