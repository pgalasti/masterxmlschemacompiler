package com.galasti.xmlcompiler.structure.document;

import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;

/**
 * Extends off {@link XmlDocument} and implements get methods from 
 * {@link IXmlReader} to read elements from the XmlDocument. Elements read from
 * the object will be copies to not modify original document.
 * <br/>
 * @author pgalasti
 *
 */
public class XmlDocumentReader extends XmlDocument implements IXmlReader {

	/**
	 * Constructs an XML document with given root element and body elements.
	 * @param xmlRootElement Root element of XML document
	 * @param xmlTopElement Top body element
	 */
	public XmlDocumentReader(final XmlRootElement xmlRootElement, final XmlBodyElement xmlTopElement) {
		super(xmlRootElement, xmlTopElement);
	}
	
	/**
	 * Returns a copy of the root element from the document.
	 */
	@Override
	public XmlRootElement getRootElement() {
		return this.xmlRootElement.clone();
	}

	/**
	 * Returns a copy of the top body element.
	 */
	@Override
	public XmlBodyElement getBodyElement() {
		return this.xmlTopElement.clone();
	}

}
