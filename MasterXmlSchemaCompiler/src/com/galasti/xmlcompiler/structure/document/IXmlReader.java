package com.galasti.xmlcompiler.structure.document;

import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;

/**
 * Interface to read elements off of a document.
 * @author pgalasti
 *
 */
public interface IXmlReader {

	/**
	 * Reads the root element of a document.
	 * @return The root element of a document.
	 */
	public XmlRootElement getRootElement();
	
	/**
	 * Reads the top body element of a document.
	 * @return The body element of a document.
	 */
	public XmlBodyElement getBodyElement();
}
