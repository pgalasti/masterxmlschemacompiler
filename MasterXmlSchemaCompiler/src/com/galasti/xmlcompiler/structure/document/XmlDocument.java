package com.galasti.xmlcompiler.structure.document;

import com.galasti.xmlcompiler.settings.Settings;
import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;

/**
 * Represents an XML structured document.
 * @author pgalasti
 *
 */
public abstract class XmlDocument {

	protected XmlRootElement xmlRootElement;
	protected XmlBodyElement xmlTopElement;
	
	public XmlDocument() {
		this.xmlRootElement = new XmlRootElement(Settings.getXmlVersion(), Settings.getEncoding());
		this.xmlTopElement = new XmlBodyElement("");
	}
	
	public XmlDocument(final XmlRootElement xmlRootElement, final XmlBodyElement xmlTopElement) {
		this.xmlRootElement = xmlRootElement.clone();
		this.xmlTopElement = xmlTopElement.clone();
	}
}
