package com.galasti.xmlcompiler.structure;

/**
 * Represents the <xml> root element of an XML data structure.
 * @author pgalasti
 *
 */
public class XmlRootElement extends Element {

	public static final String VERSION_TAG = "version";
	public static final String ENCODING_TAG = "encoding";
	
	/**
	 * Constructs an XmlRootElement based on a version string and encoding 
	 * string.
	 * @param versionString The version string for the root element
	 * @param encodingString The character encoding string for the root element
	 */
	public XmlRootElement(String versionString, String encodingString) {
		super();
		
		this.attributes.put(VERSION_TAG, new Attribute(VERSION_TAG, versionString));
		this.attributes.put(ENCODING_TAG, new Attribute(ENCODING_TAG, encodingString));
	}
	
	/**
	 * Recursively will write out the XML element and children to string format
	 */
	@Override
	public String toString(ElementMetaData elementMetaData) {
		StringBuilder builder = new StringBuilder("<?xml ");
		
		// Need to make sure version is first attribute so just have this harded I guess...
		builder.append(this.attributes.get(VERSION_TAG).getAttribute())
		.append(" ")
		.append(this.attributes.get(ENCODING_TAG).getAttribute());
		
		for(Attribute attribute : this.attributes.values()) {
			// Skip these since they're hard coded
			if(attribute.getName().compareToIgnoreCase(VERSION_TAG) == 0) {
				continue;
			} else if(attribute.getName().compareToIgnoreCase(ENCODING_TAG) == 0) {
				continue;
			}
			
			builder.append(" ")
			.append(attribute.getAttribute());
		}
		
		builder.append("?>");
		
		return builder.toString();
	}
	
	@Override
	public XmlRootElement clone() {
		
		XmlRootElement rootElement;
		
		// Get attributes
		final Attribute encodingAttribute = this.attributes.get(ENCODING_TAG);
		final Attribute versionAttribute = this.attributes.get(VERSION_TAG);
		rootElement = new XmlRootElement(versionAttribute.getValue(), encodingAttribute.getValue());
		
		// Recursively get body elements
		for(Element element : this.childElements) {
			rootElement.addElement(((XmlRootElement)element).clone());
		}
		return rootElement;
	}
}
