package com.galasti.xmlcompiler.console;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.galasti.xmlcompiler.merger.XsdIncludeMerger;
import com.galasti.xmlcompiler.settings.Settings;
import com.galasti.xmlcompiler.structure.XmlBodyElement;

public class XsdMergeDriver {

	public final static String USAGE =
			"\n1 - Documentation.properties"+
					"\n2 - MasterElement.properties"+
					"\n3 - InputFile.xsd"+
					"\n4 - OutputFile.xsd"+
					"\n5 - custom.properties (OPTIONAL)";

	/**
	 * Executes XSD merge operation to an output file.
	 * @param args 
	 * <br/>
	 * 1 - <b>Documentation.properties</b> - Full path to properties file 
	 * containing documentation element properties.
	 * <br/>
	 * 2 - <b>MasterElement.properties</b> - Full path to properties file
	 * containing master element and grouping
	 * <br/>
	 * 3 - <b>InputFile.xsd</b> Full path to input file with all references to be parsed
	 * <br/>
	 * 4 - <b>OutputFile.xsd</b> Full path to output file with merged results
	 * <br/>
	 * 5 - <b>custom.properties (optional)</b> Custom settings.properties path 
	 */
	public static void main(String[] args) throws IOException {

		if(args.length < 4 || args.length > 5 ) {
			throw new RuntimeException("XsdMergeDriver requires arguments: " + USAGE);
		}
		
		Properties documentElementProperties = new Properties();
		Properties masterElementProperties = new Properties();

		final String documentPropertiesPath = args[0];
		final String masterPropertiesPath = args[1];
		final String inputFilePath = args[2];
		final String outputFilePath = args[3];
		
		String customPropertiesPath = "";
		if(args.length > 4) {
			customPropertiesPath = args[4];
		}
		
		// Load custom properties if specified
		if(!customPropertiesPath.isEmpty()) {
			Settings.loadCustomProperties(customPropertiesPath);
		}
		
		// Load properties objects
		System.out.println("Loading XSD merge properties...");
		InputStream documentInputStream = new FileInputStream(documentPropertiesPath);
		InputStream masterInputStream = new FileInputStream(masterPropertiesPath);
		
		documentElementProperties.load(documentInputStream);
		masterElementProperties.load(masterInputStream);
		
		XmlBodyElement element = new XmlBodyElement("");
		XsdIncludeMerger merger = new XsdIncludeMerger(inputFilePath, element);
		
		merger.setDocumentationElementsProperties(documentElementProperties);
		merger.setMasterElementProperties(masterElementProperties);
		
		System.out.println("Performing XSD merge...");
		merger.merge(outputFilePath);
		
		System.out.println("XSD merge complete.");
		System.out.println("Merged File: " + outputFilePath);
	}

}
