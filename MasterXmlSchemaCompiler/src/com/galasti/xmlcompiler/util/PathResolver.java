package com.galasti.xmlcompiler.util;

import java.io.File;
import java.io.IOException;

/**
 * Utility class to resolve paths of files/directories.
 * @author pgalasti
 *
 */
public class PathResolver {

	/**
	 * Returns a relative path based on an implicit string.
	 * @param path Implicit path
	 * @return A file pointing to the relative path specified
	 */
	public static File getRelativeDirectory(String path) {
		
		if(!new File(path).exists()) {
			throw new RuntimeException("File with path: " + path + " does not exist!");
		}

		return new File(new File(resolve(path)).getParent());
	}
	
	/**
	 * Returns the directory of the specified file path
	 * @param filePath A full file path
	 * @return A file path directory of a file path
	 */
	public static String getDirectory(String filePath) {
		return new File(filePath).getParent() + File.separator;
	}
	
	private final static char WINDOWS_SEPARATOR = '\\';
	private final static char UNIX_SEPARATOR = '/';
	
	/**
	 * Parses and returns the file at the file path.
	 * @param filePath The file path of a file
	 * @return The file name text.
	 */
	public static String getFileFromUri(String filePath) {
		int startIndex = filePath.lastIndexOf(UNIX_SEPARATOR)+1;
		if(startIndex == -1) {
			startIndex = filePath.lastIndexOf(WINDOWS_SEPARATOR)+1;
		}

		return filePath.substring(startIndex);
	}
	
	private static String resolve(String path) {
		try {
			return new File(path).getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Unable to determine canonical path: " + path, e);
		}
	}
}
