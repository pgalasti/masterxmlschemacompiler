package com.galasti.xmlcompiler.settings;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Returns settings properties.
 * @author pgalasti
 *
 */
public class Settings {

	private static Properties propertiesInstance;
	
	private static String DEFAULT_FILE = "settings.properties";
	
	protected static Properties getInstance() {
		
		// Default
		if(propertiesInstance == null) {
			
			loadDefaultProperties();
		}
		
		return propertiesInstance;
	}
	
	/**
	 * Loads the default properties file embedded with application
	 */
	public static void loadDefaultProperties() {
		load(DEFAULT_FILE);
	}
	
	/**
	 * Loads a custom properties file for the application.
	 * @param path Path to the custom properties file.
	 */
	public static void loadCustomProperties(String path) {
		load(path);
	}
	
	/**
	 * Returns the encoding property of the loaded properties file.
	 * @return The 'encoding' property
	 */
	public static String getEncoding() {
		
		if(propertiesInstance == null) {
			loadDefaultProperties();
		}
		
		return (String) propertiesInstance.get("encoding");
	}
	
	/**
	 * Returns the XML version property of the loaded properties file.
	 * @return The 'xmlVersion' property
	 */
	public static String getXmlVersion() {
		
		if(propertiesInstance == null) {
			loadDefaultProperties();
		}
		
		return (String) propertiesInstance.get("xmlVersion");
	}
	
	/**
	 * Properties file should be setup to return a 'Y' to enable tab depth 
	 * when formatting files.
	 * @return The 'tabDepth' property
	 */
	public static char getTabDepthEnable() {
		
		if(propertiesInstance == null) {
			loadDefaultProperties();
		}
		
		return  ((String) propertiesInstance.get("tabDepth")).charAt(0);
	}
	
	/**
	 * Returns the default namespace to use for default elements.
	 * @return The 'defaultNamespace' property
	 */
	public static String getDefaultNamespace() {
		
		if(propertiesInstance == null) {
			loadDefaultProperties();
		}
		
		return (String) propertiesInstance.get("defaultNamespace");
	}
	
	protected static void load(String path) {
		
		InputStream input = ClassLoader.getSystemResourceAsStream(path);
		
		try {
			propertiesInstance = new Properties();
			propertiesInstance.load(input);
		} catch (IOException e) {
			throw new RuntimeException("Unable to load default properties file. ", e);
		}
	}
	
}
