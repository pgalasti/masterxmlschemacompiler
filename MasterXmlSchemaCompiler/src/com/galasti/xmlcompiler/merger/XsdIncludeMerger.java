package com.galasti.xmlcompiler.merger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.galasti.xmlcompiler.parser.XmlParser;
import com.galasti.xmlcompiler.settings.Settings;
import com.galasti.xmlcompiler.structure.Attribute;
import com.galasti.xmlcompiler.structure.Element;
import com.galasti.xmlcompiler.structure.Tag;
import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.XmlRootElement;
import com.galasti.xmlcompiler.structure.document.XmlDocumentReader;
import com.galasti.xmlcompiler.structure.document.XmlDocumentWriter;

/**
 * Merges XSD files specified with include elements. Will recursively search 
 * for all all includes. If an include had already be found in a subsequent 
 * element search, it will only be added once to the output file.
 * @author pgalasti
 *
 */
public class XsdIncludeMerger extends Merger{

	private IncludeSearcher includeSearcher;
	private Properties documentationProperties;
	private Properties masterElementProperties;
	
	// Helper tags
	private final Tag INCLUDE_TAG = new Tag(Settings.getDefaultNamespace(), "include");
	private final Tag SCHEMA_TAG = new Tag(Settings.getDefaultNamespace(), "schema");
	private final Tag ELEMENT_TAG = new Tag(Settings.getDefaultNamespace(), "element");
	private final Tag SEQUENCE_TAG = new Tag(Settings.getDefaultNamespace(), "sequence");
	private final Tag COMPLEX_TYPE_TAG = new Tag(Settings.getDefaultNamespace(), "complexType");
	private final Tag ANNOTATION_TAG = new Tag(Settings.getDefaultNamespace(), "annotation");
	private final Tag DOCUMENTATION_TAG = new Tag(Settings.getDefaultNamespace(), "documentation");
	
	/**
	 * Constructs an XsdIncludeMerger object
	 * @param filePath File path containing the XSD includes.
	 * @param masterElement Master element of new merged XSD file.
	 */
	public XsdIncludeMerger(String filePath, XmlBodyElement masterElement) {
		super(filePath, masterElement);
		this.masterElement = new XmlBodyElement(this.SCHEMA_TAG);
		this.includeSearcher = new IncludeSearcher(this.filePath, Settings.getDefaultNamespace());
	}
	
	@Override
	public void merge(String outputPath) {

		XmlParser parser;

		// Get the include elements
		Map<String, XmlBodyElement> includeElements = this.includeSearcher.findIncludeElements();
		XmlBodyElement topMergedBodyElement = masterElement;
		topMergedBodyElement.addElement(createDocumentationElement());
		topMergedBodyElement.addElement(createMasterLayoutElement());

		List<String> schemaReferences = getReferenceNames(includeElements.keySet());
		topMergedBodyElement.addElement(
				this.createGroupElement(schemaReferences));

		
		
		// For each include element merge it's data
		for(String fileInclude : includeElements.keySet()) {

			// Create a new parser for that include path and parse
			parser = new XmlParser(new File(fileInclude));
			XmlDocumentReader reader = parser.parseFile();

			// Get the top most body element
			XmlBodyElement bodyElement = reader.getBodyElement();

			// Find the schema element to be removed per schema file (maybe perform validation against other schemas)
			XmlBodyElement rootSchemaTagElement = bodyElement.find(this.SCHEMA_TAG); 
			if(rootSchemaTagElement == null) {
				throw new RuntimeException("Unable to find root body schema tag");
			}

			// Copy over the attributes to the top merged schema element
			for(Attribute attribute : rootSchemaTagElement.getAttributes()) {
				topMergedBodyElement.addAttribute(attribute);
			}

			// Copy the child elements of the schema to the new schema
			for(Element childElement : rootSchemaTagElement.getChildElements()) {
				XmlBodyElement element = ((XmlBodyElement)childElement);
				topMergedBodyElement.addElement(element.clone());
			}

			XmlBodyElement includeElement = topMergedBodyElement.find(this.INCLUDE_TAG);
			if(includeElement != null) {
				topMergedBodyElement.remove(includeElement);
			}
		}

		this.masterElement = topMergedBodyElement;

		XmlDocumentWriter writer = new XmlDocumentWriter(outputPath);
		writer.setRootElement(new XmlRootElement(Settings.getXmlVersion(), Settings.getEncoding()));
		writer.setTopElement(topMergedBodyElement);

		writer.writeToFile();
	}

	/**
	 * Sets the documentation element properties.
	 * @param property Documentation properties.
	 */
	public void setDocumentationElementsProperties(Properties property) {
		this.documentationProperties = property;
	}

	/**
	 * Sets the master element properties.
	 * @param property Master element properties
	 */
	public void setMasterElementProperties(Properties property) {
		this.masterElementProperties = property;
	}
	
	private XmlBodyElement createDocumentationElement() {

		XmlBodyElement annotationElement = new XmlBodyElement(this.ANNOTATION_TAG);
		XmlBodyElement documentationElement = new XmlBodyElement(this.DOCUMENTATION_TAG);
		annotationElement.addElement(documentationElement);

		Set<Object> properties = this.documentationProperties.keySet();
		for(Object key : properties) {
			String keyString = ((String)key);
			String content = (String) this.documentationProperties.get(key);
			
			XmlBodyElement description = new XmlBodyElement(keyString);
			description.setContent(content);
			documentationElement.addElement(description);
		}
		
		return annotationElement;
	}

	private XmlBodyElement createMasterLayoutElement() {

		XmlBodyElement layoutElement = new XmlBodyElement(this.ELEMENT_TAG);
		
		String elementName = (String) this.masterElementProperties.get("name");
		layoutElement.addAttribute(new Attribute("name", elementName));

		XmlBodyElement complexTypeElement = new XmlBodyElement(this.COMPLEX_TYPE_TAG);
		layoutElement.addElement(complexTypeElement);

		XmlBodyElement sequenceElement = new XmlBodyElement(this.SEQUENCE_TAG);
		complexTypeElement.addElement(sequenceElement);

		// list through groups here; hard coded for now
		XmlBodyElement groupElement = new XmlBodyElement(this.ELEMENT_TAG);
		sequenceElement.addElement(groupElement);

		String groupName = (String) this.masterElementProperties.get("groupName");
		String groupType = (String) this.masterElementProperties.get("groupType");
		
		groupElement.addAttribute(new Attribute("maxOccurs", "1"));
		groupElement.addAttribute(new Attribute("minOccurs", "1"));
		groupElement.addAttribute(new Attribute("name", groupName));
		groupElement.addAttribute(new Attribute("type", groupType));

		return layoutElement;
	}

	private List<String> getReferenceNames(Set<String> filePaths) {

		List<String> elementNames = new ArrayList<>();
		XmlParser parser = null;
		for(String fileInclude : filePaths) {

			// Create a new parser for that include path and parse
			parser = new XmlParser(new File(fileInclude));
			XmlDocumentReader reader = parser.parseFile();
			XmlBodyElement bodyElement = reader.getBodyElement();

			XmlBodyElement foundElement = bodyElement.find(this.ELEMENT_TAG);
			if(foundElement == null) {
				continue;
			}

			String name = foundElement.findAttribute("name");
			if(name.isEmpty()) {
				continue;
			}

			elementNames.add(name);
		}

		return elementNames;
	}

	private XmlBodyElement createGroupElement(List<String> references) {

		String groupType = (String) this.masterElementProperties.get("groupType");
		XmlBodyElement complexTypeElement = new XmlBodyElement(this.COMPLEX_TYPE_TAG);
		complexTypeElement.addAttribute(new Attribute("name", groupType)); // hard coded for now

		XmlBodyElement sequenceElement = new XmlBodyElement(this.SEQUENCE_TAG);
		complexTypeElement.addElement(sequenceElement);

		for(String reference : references) {
			XmlBodyElement referenceElement = new XmlBodyElement(ELEMENT_TAG);
			referenceElement.addAttribute(new Attribute("maxOccurs", "1"));
			referenceElement.addAttribute(new Attribute("minOccurs", "1"));
			referenceElement.addAttribute(new Attribute("ref", reference));

			sequenceElement.addElement(referenceElement);
		}

		return complexTypeElement;
	}
}
