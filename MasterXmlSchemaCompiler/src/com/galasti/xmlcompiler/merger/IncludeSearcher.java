package com.galasti.xmlcompiler.merger;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.galasti.xmlcompiler.parser.XmlParser;
import com.galasti.xmlcompiler.structure.Element;
import com.galasti.xmlcompiler.structure.Tag;
import com.galasti.xmlcompiler.structure.XmlBodyElement;
import com.galasti.xmlcompiler.structure.document.XmlDocumentReader;
import com.galasti.xmlcompiler.util.PathResolver;

/**
 * Searches for schema include tags in an XML document.
 * @author pgalasti
 */
public class IncludeSearcher {

	public static String INCLUDE_TAG_NAME = "include";
	public static String SCHEMA_LOCATION_ATTRIBUTE_NAME = "schemaLocation";
	
	private XmlParser parser;
	private String filePath;
	private String namespace;
	
	/**
	 * Constructs a searcher object with a document and namespace the include 
	 * statement is defined under.
	 * @param document The document reader
	 * @param filePath File path of the document reader
	 * @param namespace The namespace the include statement is defined under
	 */
	public IncludeSearcher(String filePath, String namespace) {
		this.parser = new XmlParser(new File(filePath));
		this.filePath = filePath;
		this.namespace = namespace;
	}
	
	/**
	 * Finds all include statements embedded in an XML document.
	 * @return Returns a map with the file name as key and include element as
	 * the payload
	 */
	public Map<String, XmlBodyElement> findIncludeElements() {
		
		XmlDocumentReader document = parser.parseFile();
		final XmlBodyElement bodyElement = document.getBodyElement();

		// <File Name, Element>
		// Put it in a map in case children have same reference to file.
		Map<String, XmlBodyElement> includeMap = new HashMap<>();
		includeMap = this.recursivelyExtractIncludes(includeMap, bodyElement);
		
		return includeMap;
	}
	
	private Map<String, XmlBodyElement> recursivelyExtractIncludes(Map<String, XmlBodyElement> map, XmlBodyElement nextElement) {
		
		List<Element> childElements = nextElement.getChildElements();
		for(Element element : childElements) {
			
			XmlBodyElement childElement = ((XmlBodyElement)element);
			
			// If the tag matches the namespace:include tag
			
			Tag tag = new Tag(this.namespace, INCLUDE_TAG_NAME);
			if(childElement.getTag().compare(tag)) {
				
				// Find the schemaLocation attribute name.
				final String schemaValue = childElement.findAttribute(SCHEMA_LOCATION_ATTRIBUTE_NAME);
				if(schemaValue.isEmpty()) {
					throw new RuntimeException("schemaLocation attribute has been found, but value is blank!");
				}
				
				final String startingDirectory = PathResolver.getDirectory(this.filePath);
				String fullFilePath = PathResolver.getRelativeDirectory(startingDirectory + schemaValue).getAbsolutePath() + File.separator;

				
				fullFilePath += PathResolver.getFileFromUri(schemaValue);
				map.put(fullFilePath, childElement);
				
				// Recursively find includes in subsequent files
				IncludeSearcher includeSearcher = new IncludeSearcher(fullFilePath, this.namespace);
				map.putAll(includeSearcher.findIncludeElements());
				continue;
			}
			
			// Put the recursive results in the returned map
			map.putAll(recursivelyExtractIncludes(map, childElement));
		}
		
		return map;
	}
}
