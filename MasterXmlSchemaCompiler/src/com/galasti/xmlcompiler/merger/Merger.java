package com.galasti.xmlcompiler.merger;

import com.galasti.xmlcompiler.structure.XmlBodyElement;

/**
 * Abstract class for merging XML type documents. Extended children should have 
 * specific implementations for the specified type.
 * @author pgalasti
 *
 */
public abstract class Merger {

	protected XmlBodyElement masterElement;
	protected String filePath;
	
	/**
	 * Creates a Merger object
	 * @param filePath File path of the main file
	 * @param masterElement Master element of the new merged file
	 */
	public Merger(String filePath, XmlBodyElement masterElement) {
		this.filePath = filePath;
		this.masterElement = masterElement.clone();
	}
	
	/**
	 * Merges included XML files into the output path
	 * @param outputPath The output path of the new file
	 */
	public abstract void merge(String outputPath);
	
}
