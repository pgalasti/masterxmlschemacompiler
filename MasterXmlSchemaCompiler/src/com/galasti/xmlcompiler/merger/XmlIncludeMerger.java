package com.galasti.xmlcompiler.merger;

import com.galasti.xmlcompiler.structure.XmlBodyElement;

/**
 * Merges Xml documents based on include elements in an XML file.
 * Not currently implemented
 * @author pgalasti
 *
 */
public class XmlIncludeMerger extends Merger {

	/**
	 * Not implemented. Will throw UnsupportedOperationException()
	 * @param filePath
	 * @param masterElement
	 */
	public XmlIncludeMerger(String filePath, XmlBodyElement masterElement) {
		super(filePath, masterElement);
		throw new UnsupportedOperationException("Not implemented");
	}

	/**
	 * Not implemented. Will throw UnsupportedOperationException()
	 */
	@Override
	public void merge(String outputPath) {
		throw new UnsupportedOperationException("Not implemented");
	}

}
